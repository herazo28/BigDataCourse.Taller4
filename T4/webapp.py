# -*- coding: utf-8 -*-

from flask import Flask, request
from flask import render_template
from bson.objectid import ObjectId

import mongoQuery
import RSS_Reader

# Conexión a mongo
import MongoConn


app = Flask(__name__)

@app.route('/')
def index():
    return render_template('general/grupoinfo.html')
    
@app.route('/getQuestions')
def getQuestions():
    return str(MongoConn.getQuestions())

@app.route('/pregunta/preguntas')    
@app.route('/preguntas')
def preguntas():
    listado = MongoConn.getQuestions()
    return render_template('general/preguntas.html', preguntas=listado)

@app.route('/pregunta/<string:pregunta_id>')
def pregunta(pregunta_id):
    data = MongoConn.getQuestion(pregunta_id)    
    
    return render_template('general/pregunta.html', pregunta=data)

@app.route('/busqueda', methods=['GET', 'POST'])
def busqueda():
    if request.method == 'POST':
        searchTerms = request.form.get('searchTerms')        
               
        registros = mongoQuery.query(searchTerms)
        
        return render_template('general/busqueda.html', registros=registros, searchTerms=searchTerms)
    else:
        return render_template('general/busqueda.html')
        
@app.route('/RSS_download')
def processRSS():
    resultado = RSS_Reader.processRSS()
    return 'Registros insertados: ' + str(resultado[0])


app.run(debug=True, host ='0.0.0.0', port=8083)

