/* ---------- Pie chart ---------- */
	var data = [
	{ label: "Negativo",  data: 290},
	{ label: "Neutro",  data: 305},
	{ label: "Positivo",  data: 405}
	];

	if($("#polaridadTwitter1000").length)
	{
		$.plot($("#polaridadTwitter1000"), data,
		{
			series: {
					pie: {
							show: true
					}
			},
			grid: {
					hoverable: true,
					clickable: true
			},
			legend: {
				show: false
			},
			colors: ["#FA5833", "#2FABE9", "#FABB3D"]
		});
		
		function pieHover(event, pos, obj)
		{
			if (!obj)
					return;
			percent = parseFloat(obj.series.percent).toFixed(2);
			$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
		}
		$("#polaridadTwitter1000").bind("plothover", pieHover);
	}

/* ---------- Pie chart ---------- */
	var data = [
	{ label: "Anger",  data: 95},
	{ label: "Disgust",  data: 95},
	{ label: "Irony",  data: 79},
	{ label: "Fear",  data: 0},
	{ label: "Joy",  data: 239},
	{ label: "No-emotion",  data: 348},
	{ label: "Sadness",  data: 136},
	{ label: "Surprise",  data: 8}
	];

	
	if($("#sentimientosTwitter1000").length)
	{
		$.plot($("#sentimientosTwitter1000"), data,
		{
			series: {
					pie: {
							show: true
					}
			},
			grid: {
					hoverable: true,
					clickable: true
			},
			legend: {
				show: false
			},
			colors: ["#FA5833", "#2FABE9", "#FABB3D", "#BDBDBD", "#FA5858", "#BCF5A9", "#01DFA5", "#0000FF"]
		});
		
		function pieHover(event, pos, obj)
		{
			if (!obj)
					return;
			percent = parseFloat(obj.series.percent).toFixed(2);
			$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
		}
		$("#sentimientosTwitter1000").bind("plothover", pieHover);
	}

/* ---------- Pie chart ---------- */
	var data = [
	{ label: "Negativo",  data: 368},
	{ label: "Neutro",  data: 97},
	{ label: "Positivo",  data: 735}
	];

	if($("#polaridadFacebook1200").length)
	{
		$.plot($("#polaridadFacebook1200"), data,
		{
			series: {
					pie: {
							show: true
					}
			},
			grid: {
					hoverable: true,
					clickable: true
			},
			legend: {
				show: false
			},
			colors: ["#FA5833", "#2FABE9", "#FABB3D"]
		});
		
		function pieHover(event, pos, obj)
		{
			if (!obj)
					return;
			percent = parseFloat(obj.series.percent).toFixed(2);
			$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
		}
		$("#polaridadFacebook1200").bind("plothover", pieHover);
	}

/* ---------- Pie chart ---------- */
	var data = [
	{ label: "Anger",  data: 82},
	{ label: "Disgust",  data: 138},
	{ label: "Irony",  data: 6},
	{ label: "Fear",  data: 5},
	{ label: "Joy",  data: 255},
	{ label: "No-emotion",  data: 91},
	{ label: "Sadness",  data: 143},
	{ label: "Surprise",  data: 480}
	];

	
	if($("#sentimientosFacebook1200").length)
	{
		$.plot($("#sentimientosFacebook1200"), data,
		{
			series: {
					pie: {
							show: true
					}
			},
			grid: {
					hoverable: true,
					clickable: true
			},
			legend: {
				show: false
			},
			colors: ["#FA5833", "#2FABE9", "#FABB3D", "#BDBDBD", "#FA5858", "#BCF5A9", "#01DFA5", "#0000FF"]
		});
		
		function pieHover(event, pos, obj)
		{
			if (!obj)
					return;
			percent = parseFloat(obj.series.percent).toFixed(2);
			$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
		}
		$("#sentimientosFacebook1200").bind("plothover", pieHover);
	}

/* ---------- Chart with points ---------- */
	if($("#comparacionPolaridad").length)
	{
		var sin = [], cos = [];
		sin = [[1, 55], [2, 56], [3, 55], [4, 58], [5, 44], [6, 66], [7, 57], [8, 47], [9, 99], [10, 99]]
		cos = [[1, 64], [2, 77.5], [3, 66.6], [4, 70], [5, 77.5], [6, 73.3], [7, 75], [8, 72.5], [9, 64], [10, 72.5]]
          

		var plot = $.plot($("#comparacionPolaridad"),
			   [ { data: sin, label: "Tweets1000"}, { data: cos, label: "Facebook1200" } ], {
				   series: {
					   lines: { show: true,
								lineWidth: 2,
							 },
					   points: { show: true },
					   shadowSize: 2
				   },
				   grid: { hoverable: true, 
						   clickable: true, 
						   tickColor: "#dddddd",
						   borderWidth: 0 
						 },
				   yaxis: { min: 0, max: 100 },
				   colors: ["#FA5833", "#2FABE9"]
				 });

		function showTooltip(x, y, contents) {
			$('<div id="tooltip">' + contents + '</div>').css( {
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x + 5,
				border: '1px solid #fdd',
				padding: '2px',
				'background-color': '#dfeffc',
				opacity: 0.80
			}).appendTo("body").fadeIn(200);
		}

		var previousPoint = null;
		$("#comparacionPolaridad").bind("plothover", function (event, pos, item) {
			$("#x").text(pos.x.toFixed(2));
			$("#y").text(pos.y.toFixed(2));

				if (item) {
					if (previousPoint != item.dataIndex) {
						previousPoint = item.dataIndex;

						$("#tooltip").remove();
						var x = item.datapoint[0].toFixed(2),
							y = item.datapoint[1].toFixed(2);

						showTooltip(item.pageX, item.pageY,
									item.series.label + " of " + x + " = " + y);
					}
				}
				else {
					$("#tooltip").remove();
					previousPoint = null;
				}
		});
		


		$("#comparacionPolaridad").bind("plotclick", function (event, pos, item) {
			if (item) {
				$("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
				plot.highlight(item.series, item.datapoint);
			}
		});
	}


/* ---------- Chart with points ---------- */
	if($("#comparacionSentimientos").length)
	{
		var sin = [], cos = [];
		sin = [[1, 63], [2, 62], [3, 57], [4, 47], [5, 56], [6, 45], [7, 57], [8, 35], [9, 99], [10, 99]]
		cos = [[1, 41.6], [2, 55.8], [3, 38.3], [4, 45], [5, 50], [6, 49.2], [7, 47.5], [8, 40.8], [9, 35.8], [10, 46.7]]
          

		var plot = $.plot($("#comparacionSentimientos"),
			   [ { data: sin, label: "Tweets1000"}, { data: cos, label: "Facebook1200" } ], {
				   series: {
					   lines: { show: true,
								lineWidth: 2,
							 },
					   points: { show: true },
					   shadowSize: 2
				   },
				   grid: { hoverable: true, 
						   clickable: true, 
						   tickColor: "#dddddd",
						   borderWidth: 0 
						 },
				   yaxis: { min: 0, max: 100 },
				   colors: ["#FA5833", "#2FABE9"]
				 });

		function showTooltip(x, y, contents) {
			$('<div id="tooltip">' + contents + '</div>').css( {
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x + 5,
				border: '1px solid #fdd',
				padding: '2px',
				'background-color': '#dfeffc',
				opacity: 0.80
			}).appendTo("body").fadeIn(200);
		}

		var previousPoint = null;
		$("#comparacionSentimientos").bind("plothover", function (event, pos, item) {
			$("#x").text(pos.x.toFixed(2));
			$("#y").text(pos.y.toFixed(2));

				if (item) {
					if (previousPoint != item.dataIndex) {
						previousPoint = item.dataIndex;

						$("#tooltip").remove();
						var x = item.datapoint[0].toFixed(2),
							y = item.datapoint[1].toFixed(2);

						showTooltip(item.pageX, item.pageY,
									item.series.label + " of " + x + " = " + y);
					}
				}
				else {
					$("#tooltip").remove();
					previousPoint = null;
				}
		});
		


		$("#comparacionSentimientos").bind("plotclick", function (event, pos, item) {
			if (item) {
				$("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
				plot.highlight(item.series, item.datapoint);
			}
		});
	}