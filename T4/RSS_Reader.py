# -*- coding: utf-8 -*-

import feedparser
import MongoConn
import OpenCalais

def processRSS():
    
    # Variables para estadisticas
    numInserts = 0
    numUpdate = 0    
    
    # RSS general (ultimas 30 preguntas)
    rss_url = "https://movies.stackexchange.com/feeds"
    feed = feedparser.parse( rss_url )
    
    for entry in feed['entries']:
        question_id = entry['id']
        question_id = 'https://movies.stackexchange.com/feeds/question/' + question_id.split('/')[-1]
        last_update = entry['updated_parsed']
        
        # Se valida que no se haya insertado
        if not MongoConn.isQuestionWithID(question_id):
                        
            # Se declara el diccionario para insertar en mongo
            question_dictionary = {}
            answer_list = []  
            total_annotation = []
            
            # Se trae el RSS de la pregunta
            rss_question = feedparser.parse( question_id )
            
            # Se realiza extracción y transformación
            es_pregunta = True
            for question_entry in rss_question['entries']:
                if es_pregunta == True:
                    es_pregunta = False
                    question_dictionary['time_struct'] = rss_question['updated_parsed']
                    question_dictionary['id_rss'] = question_id
                    question_dictionary['title'] = question_entry['title']
                    question_dictionary['question'] = question_entry['summary']
                    question_dictionary['question_url'] = question_entry['link']
                    if 'tags' in question_entry:
                        question_dictionary['tags'] = question_entry['tags']
                    question_dictionary['question_annotation'] = OpenCalais.annotateWithCalais(question_entry['summary'])   
                    total_annotation = question_dictionary['question_annotation']                        
                    
                else:
                    answer_dictionary = {}
                    answer_dictionary['id_answer'] = question_entry['id']
                    answer_dictionary['title'] = question_entry['title']
                    answer_dictionary['answer'] = question_entry['summary']
                    answer_dictionary['answer_annotation'] = OpenCalais.annotateWithCalais(question_entry['summary'])   
                    answer_list.append(answer_dictionary)
                    total_annotation += answer_dictionary['answer_annotation']
            
            if len(total_annotation) > 0:
                question_dictionary['total_annotation'] = total_annotation               
            if len(answer_list) > 0:
                question_dictionary['answers'] = answer_list
            
            # Se inserta en la base de datos
            if question_dictionary != {}:
                MongoConn.insertIntoPreguntas(question_dictionary)
                numInserts += 1
        elif MongoConn.needUpdateQuestionWithID(question_id, last_update):
            pass
            
    return [numInserts]
        
