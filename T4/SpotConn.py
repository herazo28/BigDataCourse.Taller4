# -*- coding: utf-8 -*-

import spotlight






def getEntities(pText, pUrlService="http://172.24.99.117:7444/rest/annotate", pConfidence=float(0.4), pSupport=int(20)):
    """ return a list containing the DBPedia entities of the text passed by parameter.
        
        
        Parameters:
            pUrlService : String
                url pointing to spotlight service.
            pText : String
                Text containing the entities.
            pConfidence : float
                Value required for spotlight.
            pSupport : int
                Value required for spotlight.
    """
    annotations = []
    try:
        annotations = spotlight.annotate(pUrlService, pText, confidence=float(pConfidence), support=int(pSupport))
    except spotlight.SpotlightException:
        pass
    
    return [d[u'URI'] for d in annotations]


texto = """<p>I've watched <a href=\"http://en.wikipedia.org/wiki/Memento_%28film%29\">Memento</a> quite a few times. I feel for the most part that I understand it, while most people don't. I’ve seen people go as far as to say that Lenny is Sammy Jankins which is impossible. But my interpretation is: </p>\n\n<p><em>Since Lenny applied what happened to his wife in the story of Sammy Jankins, then obviously he has to be capable of making memories after the accident or he wouldn't know all of these details thus making Lenny's condition psychological not physical and he just lies to himself to relieve his guilt and continues to believe it's physical for the same reason.</em></p>\n\n<p>But what I don’t get is the scene at the end where he's in bed with his wife and it shows his \"<em>I've done it</em>\" tattoo. So if his wife was alive to obviously see that tattoo what was his motive to kill him? And why is the tattoo not shown throughout the movie? Unless Lenny's wife <em>didn’t</em> actually die and he broke the cycle and got back with her or unless he got it removed to continue this cycle. But that doesn’t explain why his wife is alive while he has this tattoo. Can anyone explain this to me?</p>"""

entities = getEntities(texto)

print(entities)