# -*- coding: utf-8 -*-
from pymongo import MongoClient
from bson.objectid import ObjectId

def get_db():
    client = MongoClient('clusterbigdata57.virtual.uniandes.edu.co:27017')
    db = client.Grupo08
    return db
    
def get_Tweet(db):
    return db.Tweets.find_one()
    
    
def isQuestionWithID(pID):
    db = get_db()
    answer = db.Preguntas.find({"id_rss": pID })       
    
    if answer.count() > 0:
        return True
    else:
        return False

def needUpdateQuestionWithID(pID, pDatetime):
    db = get_db()
    answers = db.Preguntas.find({"id_rss": pID}, {"time_struct":1})
    
    for answer in answers:
        time_struct_ant = []
        for element in str(pDatetime).split('=')[1:]:
            time_struct_ant.append(int(element.split(',')[0].split(')')[0]))
        
        if time_struct_ant != answer["time_struct"]:
            return True
    
    return False
    
    return False

def insertIntoPreguntas(pDictionary):
    db = get_db()
    db.Preguntas.insert_one(pDictionary)
    
    
    
def getQuestions():
    respuesta = []    
    
    db = get_db()
    answers = db.MoviesQuestions.find({}, {"question":1, "answers":1})
    for answer in answers:
        respuesta.append({"id": str(answer["_id"]), "question": answer["question"][0:200], "answers": (answer["answers"] if "answers" in answer else [])})
    
    return respuesta

def getQuestion(pID):
    db = get_db()
    answers = db.MoviesQuestions.find({"_id": ObjectId(pID)}, {"id_rss": 1, "title": 1, "question": 1, "question_url": 1, "tags": 1, "question_annotation": 1, "question_annotation_nltk": 1, "total_annotation": 1, "total_annotation_nltk": 1, "answers": 1, "tweets": {"$slice": 10}, "dbpedia": 1})
    respuesta = {}    
    
    for answer in answers:
        respuesta = answer
    
    return respuesta