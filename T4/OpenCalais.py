# -*- coding: utf-8 -*-
import requests
import json

calais_url = 'https://api.thomsonreuters.com/permid/calais'
access_token = 'XJmcAAUOthDuQkGtRtFWGIFmdxOxtq0t'

def annotateWithCalais(input_data):
    entidades = []
    headers = {'X-AG-Access-Token' : access_token, 'Content-Type' : 'text/raw', 'outputformat' : 'application/json'}

    try:
        response = requests.post(calais_url, data=input_data, headers=headers, timeout=80)
    except:
        return []
    
    print ('status code: %s' % response.status_code)
    content = json.loads(response.text)
    #print ('Results received: %s' % content)
    
    for i in content:
        if not "_typeGroup" in content[i]:
            continue
    
        if content[i]["_typeGroup"] != "entities":
            continue
        
        entity = {'name': content[i]['name'], 'type': content[i]['_type']}
        entidades.append(entity)
        
    return entidades
    
input_data = '''
Pereira Risaralda Colombia Latinoamerica America
'''
print(annotateWithCalais(input_data))
    